#! /usr/bin/python3
from time import sleep
import RPi.GPIO as GPIO

## Setup the GPIO for PWM on pin16 {GPIO23}

GPIO.setwarnings(False)                             # Disable warnings
GPIO.setmode(GPIO.BCM)                              # Broadcom pin-numbering scheme
GPIO.setup(23, GPIO.OUT)                            # Set up GPIO pin 23 as an output

# Create a PWM (Pulse Width Modulation) object on GPIO pin 23 with a frequency of 220 Hz
p = GPIO.PWM(23, 220)
p.stop()                                            # Stop the PWM

# Define a list of musical note frequencies
notes = [523.25, 783.99, 830.61, 622.25]
duration = [0.5, 0.5, 0.3, 0.3]                     # Define the duration of the musical notes
wait = [0.1, 0.1, 0.1, 0.1]                         # Define the waiting time between the musical notes

## Define the 'scale playing' function
def scale(tune, duty, duration, wait):
    """Function to play the melody we created"""
    for i in range(len(tune)):
        p.ChangeFrequency(tune[i])      # Set the frequency of the PWM signal
        p.start(duty)                   # Start the PWM with the specified duty cycle
        sleep(duration[i])              # Play the note for the specified duration
        p.stop()                        # Stop the PWM
        sleep(wait[i])                  # Pause before playing the next note

# Call the scale function with the defined parameters
scale(notes, 50, duration, wait)
sleep(.7)

## Cleanup the GPIO stuff...
GPIO.cleanup()
