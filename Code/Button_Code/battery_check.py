#! /usr/bin/python3
from time import sleep
import RPi.GPIO as GPIO
from pijuice import PiJuice              # Import pijuice module
import math

pijuice = PiJuice(1, 0x14)               # Instantiate PiJuice interface object

## Setup the GPIO for PWM on pin16 {GPIO23}

GPIO.setwarnings(False)                  # Disable warnings
GPIO.setmode(GPIO.BCM)                   # Broadcom pin-numbering scheme
GPIO.setup(23, GPIO.OUT)                 # Set up GPIO pin 23 as an output

# Create a PWM (Pulse Width Modulation) object on GPIO pin 23 with a frequency of 220 Hz
p = GPIO.PWM(23, 220)               
p.stop()                                 # Stop the PWM immediately

# Define a list of musical note frequencies

notes = [32.70, 65.41, 130.81, 261.63, 523.25, 1046.50, 2093, 4186.01, 8372.02, 16744.04]

# Get the battery charge level and divide it by 10 (flooring the result)
battery = math.floor(pijuice.status.GetChargeLevel()['data']/10)

p.ChangeFrequency(notes[battery])             # Change the frequency of the PWM with the change of battery charge level
p.start(50)                             # Start the PWM with a duty cycle of 50%
sleep(1)                                # pause during 1 second
p.stop()
sleep(0.5)

## Cleanup the GPIO stuff...
GPIO.cleanup()
