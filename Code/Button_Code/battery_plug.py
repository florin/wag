#! /usr/bin/python3
from time import sleep
import RPi.GPIO as GPIO

## Setup the GPIO for PWM on pin12 {GPIO18}
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT)
p = GPIO.PWM(23, 220)
p.stop()

# Initialization of the notes we will play in the melody
notes = [523.25, 587.37, 659.26]

# Initialization of time of playing the notes
duration = [0.5, 0.5, 0.5]

# Initialization of the time between each notes
wait = [0.1, 0.1, 0.1]

## Define the 'scale playing' function
def scale(tune, duty, duration, wait):
    """Function to play the music"""
    for i in range (len(tune)):
        p.ChangeFrequency(tune[i])
        p.start(duty)
        sleep(duration[i])
        p.stop()
        sleep(wait[i])


scale(notes, 50, duration, wait)
sleep(0.7)

## Cleanup the GPIO stuff...
GPIO.cleanup()
