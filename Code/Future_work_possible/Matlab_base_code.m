rpi = raspi;

% MAIN at the begining


% Connection to the LiDAR

ser = serialport("/dev/serial0", 115200, "Timeout" ,0 );
disp("Begin setPwm, ...");

gpioPinNum   =   4 ;               % Pin for GPIO
frequency    =  50 ;               % Frequency at the bigining
dutyCycle    =  25 ;               % Duty Cycle at the bigining

setupGPIO(gpioPinNum);             % Set GPIO

Object = setGpioPinPwmMode(gpioPinNum, frequency); % Set first Mode
PWM_Start(gpioPinNum) % Start PWM

disp("End   setPwm, ...\r\n");

try 
    while   true
        distance = read_tfluna_data();                        %read the information of the LiDAR
        disp("Distance from sensor : " + string(distance) + "m");

        % Calculates the Intensity felt according to distance
        frequency = Frequency(distance);
        disp(frequency);

        % Calculate the duration
        duration = Duration_vib(frequency);
        disp(duration);

        % Create the vibration
        switch_vibration_mode(gpioPinNum, duration, frequency);
    end

catch
    % trap a CTRL+C keyboard interrupt
end

% Cleanup code
Pwm_Stop();
cleanupGpio();
ser =[]; % Close the serial port




function distance = read_tfluna_data()                       % Main function to read data from LiDAR
    while true
        counter = 1;%ser.in_waiting  ;           % count the number of bytes of the serial port
        if counter > 8
            bytes_serial = fread(ser, 9);       % read 9 bytes         
            flush(ser, buffer);                 % reset buffer
            if (bytes_serial(1) == 0x59) && (bytes_serial(2) == 0x59)        % check first two bytes : Header
                distance = bytes_serial(3) + (bytes_serial(4) * 256);        % distance in next two bytes : Distance
                distance = distance / 100 ;     % In meters
            end

        end
    end
end

% To clean and create GPIO

function setupGPIO(gpioPinNum)                          % Connection to the Haptic sensor
    configurePin(rpi, gpioPinNum, 'DigitalOutput');
end

function cleanupGpio()                                   % clear GPIO
    clear rpi;
end

% *** GPIO PWM Mode Setup and PWM Output ***

function pwmPinObject =setGpioPinPwmMode(gpioPinNum, frequency)
    pwmPinObject = configurePin(rpi, gpioPinNum,'PWM');
    writePWMFrequency(rpi, gpioPinNum, frequency);
end

function pwm_ChangeFrequency(gpioPinNum, frequency)                         % !!!
    configurePin(rpi, gpioPinNum,'PWM');
    writePWMFrequency(rpi, gpioPinNum, frequency);
end

function pwm_ChangeDutyCycle(gpioPinNum, dutyCycle)                         % !!!
    configurePin(rpi, gpioPinNum,'PWM');
    writePWMDutyCycle(rpi, gpioPinNum, dutyCycle);
end

%*** GPIO PWM Start and Stop ***

function PWM_Start(gpioPinNum)
    initDutyCycle = 50;
    pwm_ChangeDutyCycle(gpioPinNum, initDutyCycle);
end

function PWM_Stop                       % non utilisee dans les 2 codes
    clear all;
end

function switch_vibration_mode(gpioPinNum, duration, frequency)                 % Object vs gpioPinNUm!!!
    % Function to switch vibration mode
    DC = frequency/10;                   % duty cycle
    pwm_ChangeFrequency(gpioPinNum, frequency);
    pwm_ChangeDutyCycle(gpioPinNum, DC);
    pause(duration);
end

function frequence = Frequency(distance)
    %Calculates the frequency according to distance
    %Minimum distance were you feel : 3 m
    %Minimum frequency to feel 300 Hz
    %Maximum frequency possible 1000 Hz
    frequence = 1000 * (1 / exp(distance))^(1 / 3);                      % calculate the frequency
end

function duration = Duration_vib
    % Calculate the duration of the vibration
    duration = 0.1;
end
