Here you will find our code!
The principal code used for detection is the pwm_two_vibrator_python_5A.py file.

Directory Button_Code :
Codes for the button to make the speaker work :
- opening_song.py : plays a melody when the WAG (the raspberry) starts
- closing_song.py : plays a melody when the WAG (the raspberry) stops
- battery_check.py : plays the C note and changes its pitch according to the % of battery when the second button is pressed
- battery_plug.py : plays a melody when the WAG (the raspberry) is beeing plugged

Directory Future_work_possible:
Codes created for future work :
- Matlab_base_code.m : Does the vibration code but in Matlab (Still need to find a way to put on rasberryPi)
- Camera_detection_object_python_5A.py : Code camera for futur work

Hope the code will nourishes your interest.
