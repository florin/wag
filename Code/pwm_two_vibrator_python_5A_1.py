## Imports
import serial
import RPi.GPIO as GPIO
import time
import math

## Functions
def read_tfluna_data():
    """Main function to read data from LiDAR"""
    while True:
        counter = ser.in_waiting # count the number of bytes of the serial port
        if counter > 8 :
            bytes_serial = ser.read(9) # read 9 bytes
            ser.reset_input_buffer() # reset buffer

            if bytes_serial[0] == 0x59 and bytes_serial[1] == 0x59: # check first two bytes : Header
                distance = bytes_serial[2] + bytes_serial[3] * 256 # distance in next two bytes : Distance
                distance = distance / 100 # In meters
                return distance

#  *** To set up and clear GPIO ***
def setupGpio(gpioPinNum):
    """Connection to the Haptic sensor"""
    GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme
    GPIO.setup(gpioPinNum, GPIO.OUT)
    return

def cleanupGpio():
    """clear GPIO"""
    GPIO.cleanup()
    return

# *** GPIO PWM Mode Setup and PWM Output ***
def setGpioPinPwmMode(gpioPinNum, frequency):
    """Function to set the pin for the PWM"""
    pwmPinObject = GPIO.PWM(gpioPinNum, frequency)
    return pwmPinObject


def pwm_ChangeDutyCycle(pwmPinObject, DC):
    """Function to change the DutyCycle"""
    pwmPinObject.ChangeDutyCycle(DC)

#  *** GPIO PWM Start and Stop ***
def PWM_Start(pwm_Object, initDutyCycle):
    """Function to start the PWM"""
    pwm_Object.start(initDutyCycle)

def Pwm_Stop(pwmPinObject):
    """Function to stop the PWM"""
    pwmPinObject.stop()


def switch_vibration_mode(Object, duration, DC):
    """Function to switch to vibration mode"""
    pwm_ChangeDutyCycle(Object, DC)
    time.sleep(duration) # do the pwm for time of duration

def DutyCycle(distance) :
    """ Calculates the duty cycle depending on the distance, from 0 to 100% """
    if distance <= 3 :
        DC =  100 - 30 * distance
    else :
        DC = 0
    return DC


## Main
# Connection to the LiDAR
ser = serial.Serial(
    port='/dev/serial0',
    baudrate=115200,
    timeout=0
)

gpioPinNum_1   =   4 # Pin for GPIO for the first vibrator
gpioPinNum_2 = 27 # Pin for GPIO for the second vibrator
frequency_vibrators   =   1000 # Frequency at the beginning
DC    =   0 # Duty Cycle at the beginning
duration = 0.05

# Set GPIO first vibrator
setupGpio(gpioPinNum_1)
# Set GPIO second vibrator
setupGpio(gpioPinNum_2)

# Creat PWM OBJECTs :
## Second vibrator :
second_vibrator = setGpioPinPwmMode(gpioPinNum_2, frequency_vibrators) # Set first Mode of the first vibrator
PWM_Start(second_vibrator, DC) # Start PWM of the second vibrator
## First vibrator :
first_vibrator = setGpioPinPwmMode(gpioPinNum_1, frequency_vibrators) # Set first Mode of the first vibrator
PWM_Start(first_vibrator, DC) # Start PWM of the first vibrator


try:
    while True:
        distance = read_tfluna_data() #read the information of the LiDAR

        # Calculates the Intensity felt according to distance
        DC = DutyCycle(distance)

        switch_vibration_mode(second_vibrator, duration, DC)

        # Create the vibration
        switch_vibration_mode(first_vibrator, duration, DC)

except KeyboardInterrupt:          # trap a CTRL+C keyboard interrupt
    pass

finally:
    Pwm_Stop(first_vibrator)
    Pwm_Stop(second_vibrator)
    cleanupGpio()               # resets all GPIO ports used by this program
